<!DOCTYPE html>
<html lang="tr">

<head>
    <!-- Required meta tags-->
    <meta charset="utf8_turkish_ci">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Title Page-->
    <title> | Kayıt Ol |</title>

    <!-- Icons font CSS-->
    <link rel="icon" type="image/png" href="assets/images/icons/favicon.ico"/>
    <link href="assets/vendor/mdi-font/css/material-design-iconic-font.min.css" rel="stylesheet" media="all">
    <link href="assets/vendor/font-awesome-4.7/css/font-awesome.min.css" rel="stylesheet" media="all">
    <!-- Font special for pages-->
    <link href="https://fonts.googleapis.com/css?family=Poppins:100,100i,200,200i,300,300i,400,400i,500,500i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">

    <!-- Vendor CSS-->
    <link href="assets/vendor/select2/select2.min.css" rel="stylesheet" media="all">
    <link href="assets/vendor/datepicker/daterangepicker.css" rel="stylesheet" media="all">

    <!-- Main CSS-->
    <link href="assets/css/mainreg.css" rel="stylesheet" media="all">
    <!-- swet alert -->
    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
    <!-- swet alert -->
</head>

<body>
<div class="page-wrapper bg-gra-02 p-t-130 p-b-100 font-poppins">
    <div class="wrapper wrapper--w680">
        <div class="card card-4">
            <div class="card-body">
                <h2 class="title">Kayıt Formu</h2>
                <form action="inc/control.php" method="POST">
                    <div class="row row-space">
                        <div class="col-2">
                            <div class="input-group">
                                <label class="label">Adınız</label>
                                <input class="input--style-4" type="text" name="ad">
                            </div>
                        </div>
                        <div class="col-2">
                            <div class="input-group">
                                <label class="label">Soyadınız</label>
                                <input class="input--style-4" type="text" name="soyad">
                            </div>
                        </div>
                    </div>
                    <div class="row row-space">
                        <div class="col-2">
                            <div class="input-group">
                                <label class="label">Doğum Tarihiniz</label>
                                <div class="input-group-icon">
                                    <input class="input--style-4 js-datepicker" type="text" name="dtarih">
                                    <i class="zmdi zmdi-calendar-note input-icon js-btn-calendar"></i>
                                </div>
                            </div>
                        </div>
                        <!-- <div class="col-2">
                             <div class="input-group">
                                 <label class="label">Cinsiyetiniz</label>
                                 <div class="rs-select2 js-select-simple select--no-search">
                                     <select name="cinsiyet">
                                         <option disabled="disabled" selected="selected">Cinsiyetiniz.</option>
                                         <option value="Bay">Bay</option>
                                         <option value="Bayan">Bayan</option>
                                     </select>
                                     <div class="select-dropdown"></div>
                                 </div>
                             </div> -->
                        <div class="col-2">
                            <div class="input-group">
                                <label class="label">Cinsiyetiniz</label>
                                <div class="p-t-10">
                                    <label class="radio-container m-r-45">Bay
                                        <input type="radio" checked="checked" value="Bay" name="cinsiyet">
                                        <span class="checkmark"></span>
                                    </label>
                                    <label class="radio-container m-r-45">Bayan
                                        <input type="radio" value="Bayan" name="cinsiyet">
                                        <span class="checkmark"></span>
                                    </label>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row row-space">
                        <div class="col-2">
                            <div class="input-group">
                                <label class="label">Email Adresiniz</label>
                                <input class="input--style-4" type="email" name="mail">
                            </div>
                        </div>
                        <div class="col-2">
                            <div class="input-group">
                                <label class="label">Telefon Numaranız</label>
                                <input class="input--style-4" type="text" minlength="11" maxlength="11" name="tel">
                            </div>
                        </div>
                    </div>
                    <div class="input-group">
                        <label class="label">Parolanız</label>
                        <div class="rs-select2 js-select-simple select--no-search">
                            <input class="input--style-4" type="password" minlength="8" name="parola">
                            <div class="select-dropdown"></div>
                        </div>
                    </div>
                    <div class="p-t-15">
                        <button class="btn btn--radius-2 btn--blue" name="kayitol">Kayıt Ol</button>
                        &emsp; &emsp; &emsp; &emsp; &emsp; &emsp; &emsp;&emsp; &emsp;  <a href="sys-signin.php">Hesabınız varsa Giriş yapın →</a>
                    </div>
                    <br>
                    <div align="center" class="input-group">
                        <?php
                        if(@$_GET['register']=='1'){
                            echo
                            "<script type=\"text/javascript\">
                                function bekle(){
                                    window.location = \"sys-signin.php\"
                                }
                                swal(\"Kayıt Yapıldı.\", \"Giriş Sayfasına Yönlendiriliyorsunuz\", \"success\");
                                </script>
                                <body onLoad=\"setTimeout('bekle()', 5000)\">
                                ";
                        }
                        if(@$_GET['register']=='0') {
                            echo
                            "<script>swal(\"Giriş yapılamadı.\", \"Lütfen bilgilerinizi kontrol ediniz\", \"error\");</script>";
                        }
                        if(@$_GET['status']=='kayitli') {
                            echo
                            "<script>swal(\"Kayıt Tamamlanmadı.\", \"Eposta sistemde kayıtlı\", \"error\");</script>";
                        }
                        ?>
                    </div>
                    <div align="center">
                        Made with love ♥
                    </div>
            </div>
            </form>
        </div>
    </div>
</div>
</div>

<!-- Jquery JS-->
<script src="assets/vendor/jquery/jquery.min.js"></script>
<!-- Vendor JS-->
<script src="assets/vendor/select2/select2.min.js"></script>
<script src="assets/vendor/datepicker/moment.min.js"></script>
<script src="assets/vendor/datepicker/daterangepicker.js"></script>

<!-- Main JS-->
<script src="assets/js/global.js"></script>
</body>
</html>
<!-- end document-->