<?php
ob_start();
session_start();
include("inc/dbcon.php"); //veri tabanına bağlandık
$kullanicisor=$db->prepare("select * from kullanicilar where kMail=:mail");
$kullanicisor->execute(array(
    'mail' => $_SESSION['txtmail']
));
$say=$kullanicisor->rowCount();
$kullanicicek=$kullanicisor->fetch(PDO::FETCH_ASSOC);

if($say==0){
    header("location:sys-signin.php?status=unauthorized");
    exit;
}
{
    $kullanicisaysor=$db->prepare("select count(*) from kullanicilar");
    $kullanicisaysor ->execute();
    $kullanicisay=$kullanicisaysor->fetchColumn();
}
?>

<!DOCTYPE html>
<html lang="tr" dir="ltr">
  <head>
  <meta charset="utf-8" />
  <meta http-equiv="X-UA-Compatible" content="IE=edge" />
  <meta name="viewport" content="width=device-width, initial-scale=1" />

  <title>Kontrol Paneli</title>

  <!-- GOOGLE FONTS -->
  <link href="https://fonts.googleapis.com/css?family=Montserrat:400,500|Poppins:400,500,600,700|Roboto:400,500" rel="stylesheet"/>
  <link href="https://cdn.materialdesignicons.com/3.0.39/css/materialdesignicons.min.css" rel="stylesheet" />

  <!-- PLUGINS CSS STYLE -->
  <link href="assets/plugins/toaster/toastr.min.css" rel="stylesheet" />
  <link href="assets/plugins/nprogress/nprogress.css" rel="stylesheet" />
  <link href="assets/plugins/flag-icons/css/flag-icon.min.css" rel="stylesheet"/>
  <link href="assets/plugins/jvectormap/jquery-jvectormap-2.0.3.css" rel="stylesheet" />
  <link href="assets/plugins/ladda/ladda.min.css" rel="stylesheet" />
  <link href="assets/plugins/select2/css/select2.min.css" rel="stylesheet" />
  <link href="assets/plugins/daterangepicker/daterangepicker.css" rel="stylesheet" />

  <!-- SLEEK CSS -->
  <link id="sleek-css" rel="stylesheet" href="assets/css/sleek.css" />

  

  <!-- FAVICON -->
  <link href="assets/img/favicon.png" rel="shortcut icon" />

  <!--
    HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries
  -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->
  <script src="assets/plugins/nprogress/nprogress.js"></script>
</head>


  <body class="sidebar-fixed sidebar-dark header-light header-fixed" id="body">
    <script>
      NProgress.configure({ showSpinner: false });
      NProgress.start();
    </script>

    <div class="mobile-sticky-body-overlay"></div>

    <div class="wrapper">
      
              <!--
          ====================================
          ——— LEFT SIDEBAR WITH FOOTER
          =====================================
        -->
        <aside class="left-sidebar bg-sidebar">
          <div id="sidebar" class="sidebar sidebar-with-footer">
            <!-- Aplication Brand -->
            <div class="app-brand">
              <a href="sys-dashboard.php">
                <svg
                  class="brand-icon"
                  xmlns="http://www.w3.org/2000/svg"
                  preserveAspectRatio="xMidYMid"
                  width="30"
                  height="33"
                  viewBox="0 0 30 33"
                >
                  <g fill="none" fill-rule="evenodd">
                    <path
                      class="logo-fill-white"
                      fill="#7DBCFF"
                      d="M0 4v25l8 4V0zM22 4v25l8 4V0z"
                    />
                    <path class="logo-fill-white" fill="#FFF" d="M11 4v25l8 4V0z" />
                  </g>
                </svg>
                <span class="brand-name">Kontrol Paneli</span>
              </a>
            </div>
            <!-- begin sidebar scrollbar -->
            <div class="sidebar-scrollbar">

              <!-- sidebar menu -->
              <ul class="nav sidebar-inner" id="sidebar-menu">

                  <li  class="has-sub active expand" >
                    <a class="sidenav-item-link" href="javascript:void(0)" data-toggle="collapse" data-target="#dashboard"
                      aria-expanded="false" aria-controls="dashboard">
                      <i class="mdi mdi-view-dashboard-outline"></i>
                      <span class="nav-text">Kontrol Paneli</span> <b class="caret"></b>
                    </a>
                    <ul  class="collapse show"  id="dashboard"
                      data-parent="#sidebar-menu">
                      <div class="sub-menu">
                            <li  class="active" >
                              <a class="mdi mdi-monitor-dashboard"  class="sidenav-item-link" href="sys-dashboard.php">
                                  &nbsp;  <span class="nav-text">Panel</span>
                              </a>
                            </li>
                      </div>
                    </ul>
                  </li>
                  <li  class="has-sub" >
                      <a class="sidenav-item-link" href="javascript:void(0)" data-toggle="collapse" data-target="#ayarlar"
                         aria-expanded="false" aria-controls="ayarlar">
                          <i class="mdi mdi-wrench"></i>
                          <span class="nav-text">Ayarlar</span> <b class="caret"></b>
                      </a>
                      <ul  class="collapse"  id="ayarlar"
                           data-parent="#sidebar-menu">
                          <div class="sub-menu">
                              <li  class="active" >
                                  <a class="sidenav-item-link" href="#">
                                      <i class="mdi mdi-account-group"></i>&nbsp;
                                      <span class="nav-text">Kullanıcı Ayarları</span>
                                  </a>
                                  <a class="sidenav-item-link" href="#">
                                      <i class="mdi mdi-database"></i>&nbsp;
                                      <span class="nav-text">Veritabanı Ayarları</span>
                                  </a>
                              </li>

                          </div>
                      </ul>
                  </li>
                  <hr class="separator" />

                  <li  class="has-sub" >
                      <a class="sidenav-item-link" href="javascript:void(0)" data-toggle="collapse" data-target="#projelerim"
                         aria-expanded="false" aria-controls="projelerim">
                          <i class="mdi mdi-fire"></i>
                          <span class="nav-text">Projelerim</span> <b class="caret"></b>
                      </a>
                      <ul  class="collapse"  id="projelerim"
                           data-parent="#sidebar-menu">
                          <div class="sub-menu">
                              <li  class="active" >
                                 <!-- <a class="sidenav-item-link" href="#">
                                      <span class="nav-text">Örnek Sayfa</span>
                                  </a>-->
                              </li>
                              <li  class="has-sub" >
                                  <a class="sidenav-item-link" href="javascript:void(0)" data-toggle="collapse" data-target="#defterim"
                                     aria-expanded="false" aria-controls="defterim">
                                      <i class="mdi mdi-notebook"></i>&nbsp;
                                      <span class="nav-text">Defterim(v.1.0)</span> <b class="caret"></b>
                                  </a>
                                  <ul  class="collapse"  id="defterim">
                                      <div class="sub-menu">
                                          <li >
                                          <li  class="has-sub" >
                                              <a class="sidenav-item-link" href="javascript:void(0)" data-toggle="collapse" data-target="#dpanelim"
                                                 aria-expanded="false" aria-controls="dpanelim">
                                                  <i class="mdi mdi-notebook"></i>&nbsp;
                                                  <span class="nav-text">Defterim İşlemleri</span> <b class="caret"></b>
                                              </a>
                                              <ul  class="collapse"  id="dpanelim">
                                                  <div class="sub-menu">
                                                      <li >
                                                          <a href="#">Örnek 1</a>
                                                      </li>
                                                      <li >
                                                          <a href="#">Örnek 2</a>
                                                      </li>
                                                  </div>
                                              </ul>
                                             <!-- <a class="mdi mdi-notebook" href="#">&nbsp; Defterim Paneli</a>-->
                                          </li>
                                          <li >
                                              <a class="mdi mdi-finance" href="#">&nbsp; Hareket Yönetimi</a>
                                          </li>
                                          <li >
                                              <a class="mdi mdi-wallet" href="#">&nbsp; Hesap Yönetimi</a>
                                          </li>
                                      </div>
                                  </ul>
                              </li>
                          </div>
                      </ul>
                  </li>

                  <li  class="has-sub" >
                      <a class="sidenav-item-link" href="javascript:void(0)" data-toggle="collapse" data-target="#ornekler"
                         aria-expanded="false" aria-controls="ornekler">
                          <i class="mdi mdi-crown"></i>
                          <span class="nav-text">Örnekler</span> <b class="caret"></b>
                      </a>
                      <ul  class="collapse"  id="ornekler"
                           data-parent="#sidebar-menu">
                          <div class="sub-menu">
                              <li  class="active" >
                                  <a class="sidenav-item-link" href="#">
                                      <span class="nav-text">Örnek Sayfa</span>
                                  </a>
                              </li>
                              <li  class="has-sub" >
                                  <a class="sidenav-item-link" href="javascript:void(0)" data-toggle="collapse" data-target="#ornekler2"
                                     aria-expanded="false" aria-controls="ornekler2">
                                      <span class="nav-text">Açılır Pencere(Örn)</span> <b class="caret"></b>
                                  </a>
                                  <ul  class="collapse"  id="ornekler2">
                                      <div class="sub-menu">
                                          <li >
                                              <a href="#">Örnek 1</a>
                                          </li>
                                          <li >
                                              <a href="#">Örnek 2</a>
                                          </li>
                                      </div>
                                  </ul>
                              </li>
                          </div>
                      </ul>
                  </li>

              </ul>
            </div>
          </div>
        </aside>

      <div class="page-wrapper">
                  <!-- Header -->
          <header class="main-header " id="header">
            <nav class="navbar navbar-static-top navbar-expand-lg">
              <!-- Sidebar toggle button -->
              <button id="sidebar-toggler" class="sidebar-toggle">
                <span class="sr-only">Toggle navigation</span>
              </button>
              <!-- search form -->
              <div class="search-form d-none d-lg-inline-block">
                <div class="input-group">
                  <button type="button" name="search" id="search-btn" class="btn btn-flat">
                    <i class="mdi mdi-magnify"></i>
                  </button>
                  <input type="text" name="query" id="search-input" class="form-control" placeholder="..."
                         autofocus autocomplete="off" />
                </div>
                <div id="search-results-container">
                  <ul id="search-results"></ul>
                </div>
              </div>

              <div class="navbar-right ">
                <ul class="nav navbar-nav">

                  <li class="dropdown notifications-menu">
                    <button class="dropdown-toggle" data-toggle="dropdown">
                      <i class="mdi mdi-bell-outline"></i>
                    </button>
                    <ul class="dropdown-menu dropdown-menu-right">
                      <li class="dropdown-header">Bildirimler</li>
                      <li>
                        <a href="#">
                          <i class="mdi mdi-account-plus"></i> Kullanıcı Kayıt Oldu!
                          <span class=" font-size-12 d-inline-block float-right"><i class="mdi mdi-clock-outline"></i> 10 AM</span>
                        </a>
                      </li>
                      <li class="dropdown-footer">
                        <a class="text-center" href="#"> Hepsini Göster </a>
                      </li>
                    </ul>
                  </li>
                  <!-- User Account -->
                  <li class="dropdown user-menu">
                    <button href="#" class="dropdown-toggle nav-link" data-toggle="dropdown">
                      <img src="assets/img/user/user.png" class="user-image" alt="User Image" />
                      <span class="d-none d-lg-inline-block"><?php echo $kullanicicek['kAdi'],'&nbsp;',$kullanicicek['kSoyadi']?></span>
                    </button>
                    <ul class="dropdown-menu dropdown-menu-right">
                      <!-- User image -->
                      <li class="dropdown-header">
                        <img src="assets/img/user/user.png" class="img-circle" alt="User Image" />
                        <div class="d-inline-block">
                            <?php echo $kullanicicek['kAdi'],'&nbsp;',$kullanicicek['kSoyadi']?> <small class="pt-1"><?php echo $kullanicicek['kMail']?></small>
                        </div>
                      </li>

                      <li>
                        <a href="profile.html">
                          <i class="mdi mdi-account"></i> Profil
                        </a>
                      </li>
                      <li>
                        <a href="email-inbox.html">
                          <i class="mdi mdi-email"></i> Mesaj
                        </a>
                      </li>
                      <li>
                        <a href="#"> <i class="mdi mdi-settings"></i> Hesap Ayarları </a>
                      </li>

                      <li class="dropdown-footer">
                        <a href="inc/logout.php"> <i class="mdi mdi-logout"></i> Çıkış </a>
                      </li>
                    </ul>
                  </li>
                </ul>
              </div>
            </nav>
          </header>
        <div class="content-wrapper">
          <div class="content">						 
                  <!-- Top Statistics -->
                  <div class="row">
                    <div class="col-xl-3 col-sm-6">
                      <div class="card card-mini mb-4">
                        <div class="card-body">
                          <h2 class="mb-1">0</h2>
                          <p>Kayıtlı Kullanıcı</p>

                        </div>
                      </div>
                    </div>
                    <div class="col-xl-3 col-sm-6">
                      <div class="card card-mini  mb-4">
                        <div class="card-body">
                          <h2 class="mb-1">0</h2>
                          <p>Kayıtlı Hesaplar</p>

                        </div>
                      </div>
                    </div>
                    <div class="col-xl-3 col-sm-6">
                      <div class="card card-mini mb-4">
                        <div class="card-body">
                          <h2 class="mb-1">0 ₺</h2>
                          <p>Ne kadarım var ?</p>

                        </div>
                      </div>
                    </div>
                    <div class="col-xl-3 col-sm-6">
                      <div class="card card-mini mb-4">
                        <div class="card-body">
                          <h2 class="mb-1">0</h2>
                          <p>Hareketlerim</p>

                        </div>
                      </div>
                    </div>
                  </div>

						<div class="row">
							<div class="col-12"> 
                  <!-- Recent Order Table -->
                  <div class="card card-table-border-none" id="recent-orders">
                    <div class="card-header justify-content-between">
                      <h2>Son Hareketlerim</h2>
                      <div class="date-range-report ">
                        <span></span>
                      </div>
                    </div>
                    <div class="card-body pt-0 pb-5">
                      <table class="table card-table table-responsive table-responsive-large" style="width:100%">
                        <thead>
                          <tr>
                            <th>Hareket ID</th>
                            <th>Hesap Adı</th>
                            <th class="d-none d-md-table-cell">Hareket Tipi</th>
                            <th class="d-none d-md-table-cell">Tarihi</th>
                            <th class="d-none d-md-table-cell">Tutar</th>

                            <th></th>
                          </tr>
                        </thead>
                        <tbody>
                          <tr>
                            <td >1</td>
                            <td >
                              <a class="text-dark" href=""> Hesap 1</a>
                            </td>
                            <td class="d-none d-md-table-cell">Borç</td>
                            <td class="d-none d-md-table-cell">Oct 20, 2018</td>
                            <td class="d-none d-md-table-cell">₺ 200</td>

                            <td class="text-right">
                              <div class="dropdown show d-inline-block widget-dropdown">
                                <a class="dropdown-toggle icon-burger-mini" href="" role="button" id="dropdown-recent-order1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" data-display="static"></a>
                                <ul class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdown-recent-order1">
                                    <li class="dropdown-item">
                                        <a class="mdi mdi-eye" href="#"> &nbsp; Gör</a>
                                    </li>
                                   <li class="dropdown-item">
                                        <a class="mdi mdi-square-edit-outline" href="#"> &nbsp; Düzenle</a>
                                   </li>
                                  <li class="dropdown-item">
                                    <a class="mdi mdi-delete" href="#"> &nbsp; Sil</a>
                                  </li>
                                </ul>
                              </div>
                            </td>
                          </tr>
                        </tbody>
                      </table>
                    </div>
                  </div>
              </div>
            </div>
        </div>

                  <footer class="footer mt-auto">
            <div class="copyright bg-white">
              <p>
                &copy; <span id="copy-year">2019</span> Copyright Panel Template by
                <a
                  class="text-primary"
                  href="#"
                  target="_blank"
                  >isa</a
                >.
              </p>
            </div>
            <script>
                var d = new Date();
                var year = d.getFullYear();
                document.getElementById("copy-year").innerHTML = year;
            </script>
          </footer>

      </div>
    </div>

    
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDCn8TFXGg17HAUcNpkwtxxyT9Io9B_NcM" defer></script>
<script src="assets/plugins/jquery/jquery.min.js"></script>
<script src="assets/plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
<script src="assets/plugins/toaster/toastr.min.js"></script>
<script src="assets/plugins/slimscrollbar/jquery.slimscroll.min.js"></script>
<script src="assets/plugins/charts/Chart.min.js"></script>
<script src="assets/plugins/ladda/spin.min.js"></script>
<script src="assets/plugins/ladda/ladda.min.js"></script>
<script src="assets/plugins/jquery-mask-input/jquery.mask.min.js"></script>
<script src="assets/plugins/select2/js/select2.min.js"></script>
<script src="assets/plugins/jvectormap/jquery-jvectormap-2.0.3.min.js"></script>
<script src="assets/plugins/jvectormap/jquery-jvectormap-world-mill.js"></script>
<script src="assets/plugins/daterangepicker/moment.min.js"></script>
<script src="assets/plugins/daterangepicker/daterangepicker.js"></script>
<script src="assets/plugins/jekyll-search.min.js"></script>
<script src="assets/js/sleek.js"></script>
<script src="assets/js/chart.js"></script>
<script src="assets/js/date-range.js"></script>
<script src="assets/js/map.js"></script>
<script src="assets/js/custom.js"></script>
  </body>
</html>