-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Anamakine: 127.0.0.1:3306
-- Üretim Zamanı: 05 Eki 2019, 12:14:06
-- Sunucu sürümü: 5.7.26
-- PHP Sürümü: 7.2.18

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Veritabanı: `sirket`
--

-- --------------------------------------------------------

--
-- Tablo için tablo yapısı `kullanicilar`
--

DROP TABLE IF EXISTS `kullanicilar`;
CREATE TABLE IF NOT EXISTS `kullanicilar` (
  `kId` int(11) NOT NULL AUTO_INCREMENT,
  `kAdi` varchar(50) COLLATE utf8_turkish_ci NOT NULL,
  `kSoyadi` varchar(50) COLLATE utf8_turkish_ci NOT NULL,
  `kCinsiyet` varchar(5) COLLATE utf8_turkish_ci NOT NULL,
  `kParola` varchar(50) COLLATE utf8_turkish_ci NOT NULL,
  `kMail` varchar(50) COLLATE utf8_turkish_ci NOT NULL,
  `kTel` varchar(50) COLLATE utf8_turkish_ci NOT NULL,
  `kDTarihi` date NOT NULL,
  `kETarihi` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `KDurum` int(2) NOT NULL,
  `kYetki` int(4) NOT NULL,
  PRIMARY KEY (`kId`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_turkish_ci;

--
-- Tablo döküm verisi `kullanicilar`
--

INSERT INTO `kullanicilar` (`kId`, `kAdi`, `kSoyadi`, `kCinsiyet`, `kParola`, `kMail`, `kTel`, `kDTarihi`, `kETarihi`, `KDurum`, `kYetki`) VALUES
(1, 'admin', 'admin', 'Bay', '21232f297a57a5a743894a0e4a801fc3', 'admin@admin.com', '05436775222', '1993-09-08', '2019-10-04 17:36:32', 1, 0),
(2, 'Adi', 'soaydi', 'Bay', '81dc9bdb52d04dc20036dbd8313ed055', 'mail@mail.com', '05554445522', '2019-10-31', '2019-10-05 14:44:48', 1, 0),
(3, 'soda', 'soda', 'Bay', '202cb962ac59075b964b07152d234b70', 'soda@soda.com', '12345789874', '2019-10-08', '2019-10-05 14:49:57', 1, 0);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
